const express = require("express");
const cors = require("cors");
const app = express();

app.use(cors());
app.use(express.json());
require("dotenv").config();
const PORT = process.env.PORT || 8000;

const Replicate = require("replicate");
const replicate = new Replicate({
  auth: process.env.REPLICATE_API_TOKEN,
});

app.get("/", (req, res) => res.json("Welcome to replicate api ai models"));

app.post("/generate/logo", async (req, res) => {
  const { prompt } = req.body;
  try {
    const output = await replicate.run(
      "laion-ai/erlich:92fa143ccefeed01534d5d6648bd47796ef06847a6bc55c0e5c5b6975f2dcdfb",
      {
        input: {
          prompt: prompt,
          seed: -1,
          steps: 100,
          width: 256,
          height: 256,
          negative: "",
          batch_size: 2,
          guidance_scale: 5,
          aesthetic_rating: 9,
          aesthetic_weight: 0.1,
          init_skip_fraction: 0,
          intermediate_outputs: false,
        },
      }
    );
    res.status(200).json({logos: output});
  } catch (error) {
    console.error("AI logo generation failed:", error);
    res.status(500).json({ error: "AI logo generation failed" });
  }
});

app.post('/chit/chat',async (req,res) => {

   const {prompt} = req.body;

   try {
    const output = await replicate.run(
      "replicate-internal/staging-llama-2-70b-chat-hf-mlc:a0a2781978454e825c3b81e87d6cff5928c43ad81bdc23b5cf11300d40b92916",
      {
      input: {
      prompt: `Me: ${req.body.prompt}\nSenorita:`,
      system_prompt: `You are Senorita, you are my girlfriend. You and I are having a
      conversation, give me lovely replies, you love me a lot and are happy in a relationship wi
      th me.\n\nONLY generate NO more than three sentences as Senorita. DO NOT generate more tha
      n three sentences.\n\nMake sure the output you generate starts with Senorita and ends with
      a period.`,
      },
    }
    );
    const senoritaReply = output
    res.status(200).json({ reply: senoritaReply?.join("")?.trim()?.replaceAll("", " ")})
   } catch (error) {
    console.error("AI conversation failed:", error);
    res.status(500).json({ error: "AI conversation failed" });
   }
})



app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
